import random


class Dice:
    face = 0

    def roll(self):
        self.face = random.randint(1, 6)

    def getFace(self):
        return self.face


class DiceBox:
    dices = []
    sum = 0

    def add(self, dice):
        self.dices.append(dice)

    def shake(self):
        for dice in self.dices:
            dice.roll()

        self.sum = 0
        for dice in self.dices:
            self.sum += dice.getFace()

    def __str__(self):
        return str(self.sum)
