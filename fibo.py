def fib(n):
    if n <= 1:
        return n
    else:
        return fib(n - 1) + fib(n - 2)


fiblist = []
fibnum = int(input("Input number of Fibonacci number: "))
if fibnum <= 0:
    print("Plese enter a positive number")
else:
    for i in range(fibnum + 1):
        fiblist.append(str(fib(i)))
print(", ".join(fiblist))
