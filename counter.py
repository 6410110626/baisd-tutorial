class Counter:
    def __init__(self, v):
        self.v = v


class ResetableCounter(Counter):
    def __init__(self, v):
        Counter.__init__(self, v)
        self.initial = v

    def reset(self):
        self.v = self.initial
