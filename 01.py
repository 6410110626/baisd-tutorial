class Counter:
    v = 0  # field

    def inc(self):  # method
        self.v += 1


c1 = Counter()  # c1 is reference
c2 = Counter()  # c2 is reference
c1.inc()
c1.inc()
c2.inc()
print(c1.v)
print(c2.v)
