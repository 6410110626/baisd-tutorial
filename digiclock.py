class NumberDisplay:
    def __init__(self, initValue, maxValue):
        self.v = initValue
        self.max = maxValue

    def inc(self):
        self.v = (self.v + 1) % self.max

    def __str__(self):
        return "{:02d}".format(self.v)


class ClockDisplay:
    def __init__(self, hour, min):
        self.hourDisplay = NumberDisplay(hour, 24)
        self.minDisplay = NumberDisplay(min, 60)

    def tick(self):
        self.minDisplay.inc()
        if self.minDisplay.v == 0:
            self.hourDisplay.inc()

    def __str__(self):
        return str(self.hourDisplay) + ":" + str(self.minDisplay)


clock1 = ClockDisplay(9, 15)
clock2 = ClockDisplay(22, 18)
for x in range(250):
    clock1.tick()
    clock2.tick()
    print(str(clock1) + ", " + str(clock2))
