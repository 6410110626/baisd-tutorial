def bubble_sort(data: list) -> list:
    sorted_data = list(data)
    for i in range(len(sorted_data)):
        for j in range(len(sorted_data)):
            if sorted_data[i] < sorted_data[j]:
                sorted_data[i], sorted_data[j] = sorted_data[j], sorted_data[i]

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter 10 number: ").split()))

    sorted_data = bubble_sort(data)

    print(sorted_data)
